﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    ControllerInterface interfacer;

    void Start()
    {
        FindInterface();
    }

    // Update is called once per frame
    void Update()
    {
        int horizontal = 0;
        int vertical = 0;
        int atkDir = 0;

        if (Input.GetKey(InputManager.input.Left))
            horizontal -= 1;

        if (Input.GetKey(InputManager.input.Right))
            horizontal += 1;

        if (Input.GetKey(InputManager.input.Forward))
            vertical += 1;

        if (Input.GetKey(InputManager.input.Back))
            vertical -= 1;

        if (Input.GetKeyDown(InputManager.input.LeftAttack))
            atkDir -= 1;

        if (Input.GetKeyDown(InputManager.input.RightAttack))
            atkDir += 1;

        if (horizontal > 0)
            interfacer.Right();
        else if (horizontal < 0)
            interfacer.Left();

        if (vertical > 0)
            interfacer.Accelerate();
        else if (vertical < 0)
            interfacer.Brake();

        if (atkDir > 0)
            interfacer.RightAttack();
        else if (atkDir < 0)
            interfacer.LeftAttack();
    }

    void FindInterface()
    {
        foreach (Component comp in gameObject.GetComponents<Component>())
        {
            if (typeof(ControllerInterface).IsAssignableFrom(comp.GetType()))
            {
                interfacer = (ControllerInterface)comp;
                return;
            }
        }
    }
}
