﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputManager : MonoBehaviour
{
    public static InputManager input = null;

    [SerializeField] KeyCode left;
    [SerializeField] KeyCode right;
    [SerializeField] KeyCode leftAttack;
    [SerializeField] KeyCode rightAttack;
    [SerializeField] KeyCode forward;
    [SerializeField] KeyCode back;

    private void Start()
    {
        if (input != null)
            Destroy(gameObject);
        else
            input = this;
    }

    public KeyCode Left
    { get { return left; } }

    public KeyCode Right
    { get { return right; } }
    public KeyCode Forward
    { get { return forward; } }

    public KeyCode Back
    { get { return back; } }

    public KeyCode LeftAttack
    { get { return leftAttack; } }

    public KeyCode RightAttack
    { get { return rightAttack; } }
}
