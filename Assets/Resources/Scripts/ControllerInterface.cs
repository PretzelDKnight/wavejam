﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ControllerInterface
{
    void Left();

    void Right();

    void LeftAttack();

    void RightAttack();

    void Accelerate();

    void Brake();
}
