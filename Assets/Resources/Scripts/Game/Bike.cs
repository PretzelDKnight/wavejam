﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bike : MonoBehaviour, ControllerInterface
{
    [SerializeField] float maxTorque = 200;

    [SerializeField] float appliedTorque = 20;
    [SerializeField] float slowTorque = 30;

    [SerializeField] float zeroTorque = 0;

    [SerializeField] WheelCollider frontWheel;
    [SerializeField] WheelCollider backWheel;

    bool go = false;
    float accel;

    [SerializeField] GameObject leftAttack;
    [SerializeField] GameObject rightAttack;

    [SerializeField] float attackDuration;

    float attackTime;

    // Start is called before the first frame update
    void Start()
    {
        accel = 0;

        float thrust = accel * maxTorque;
        backWheel.motorTorque = thrust;
    }

    // Update is called once per frame
    void Update()
    {
        if (go)
        {
            accel += appliedTorque;

            if (accel > maxTorque)
                accel = maxTorque;
        }
        else
        {
            if (accel > zeroTorque)
                accel -= slowTorque;
            else if (accel < zeroTorque)
            {
                accel += appliedTorque;
                if (accel > zeroTorque)
                    accel = zeroTorque;
            }
        }
        
        frontWheel.motorTorque = accel * Time.deltaTime;
        backWheel.motorTorque = accel * Time.deltaTime;

        if (attackTime >= 0)
        {
            attackTime -= Time.deltaTime;
        }
        else
        {
            leftAttack.SetActive(false);
            rightAttack.SetActive(false);
        }

        go = false;
    }

    public void Accelerate()
    {
        if (!go)
            accel = 0;
        go = true;
    }

    public void Brake()
    {

    }

    public void Left()
    {

    }

    public void LeftAttack()
    {
        if (attackTime <= 0)
        {
            leftAttack.SetActive(true);
            attackTime = attackDuration;
        }
    }

    public void Right()
    {

    }

    public void RightAttack()
    {
        if (attackTime <= 0)
        {
            rightAttack.SetActive(true);
            attackTime = attackDuration;
        }
    }

}
