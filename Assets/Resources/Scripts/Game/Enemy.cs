﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum State
{
    Chase,
    RangePosition,
    Wait,
    Attack,
    Dead
}

public class Enemy : MonoBehaviour
{
    Transform target;

    [SerializeField] float movespeed;
    [SerializeField] float meleeRange;
    [SerializeField] float searchRange;
    [SerializeField] float attackInterval;
    [SerializeField] float attackDuration;

    [SerializeField] GameObject leftCollider;
    [SerializeField] GameObject rightCollider;

    [SerializeField] float timeToChase = 1f;

    [SerializeField] Text hitPrefab;

    State state;

    Vector3 tpos;
    Vector3 desired;

    float time;
    float attackTime;

    GameObject currentColl;

    [SerializeField] int health = 0;

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        state = State.Chase;
    }

    // Update is called once per frame
    void Update()
    {
        switch(state)
        {
            case State.Chase:
                desired = target.position - transform.position;
                transform.position += desired * Time.deltaTime / timeToChase;

                if (desired.sqrMagnitude <= searchRange * searchRange)
                {
                    state = State.RangePosition;

                    tpos = new Vector3(Random.Range(-5,5), 0, Random.Range(8, 12));
                }
                break;
            case State.RangePosition:
                desired = target.position + tpos - transform.position;
                transform.position += desired * Time.deltaTime / timeToChase;

                if ((transform.position - target.position + tpos).magnitude > tpos.magnitude * 3f / 4f && transform.position.z > target.position.z)
                {
                    state = State.Wait;
                }
                break;
            case State.Wait:
                transform.position += Vector3.forward * movespeed * Time.deltaTime;

                if (transform.position.z <= target.position.z)
                {
                    Vector3 pos = transform.position;
                    pos.z = target.position.z;
                    transform.position = pos;
                    time = 0;
                    state = State.Attack;
                }
                break;
            case State.Attack:
                if (time >= attackInterval)
                {
                    attackTime += Time.deltaTime;
                    currentColl.SetActive(true);

                    if (attackTime >= attackDuration)
                    {
                        currentColl.SetActive(false);
                        state = State.Wait;
                    }
                }
                else
                {
                    time += Time.deltaTime;
                    attackTime = 0;

                    if (target.transform.position.x < transform.position.x)
                        currentColl = leftCollider;
                    else
                        currentColl = rightCollider;
                }
                break;
            default:
                break;
        }

        if (health <= 0)
        {
            state = State.Dead;
            StartCoroutine(Death());
        }

        Debug.Log(state.ToString()) ;
    }

    IEnumerator Death()
    {
        yield return null;
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerAttack")
        {
            if (BPerM.ReturnBeat != Beat.Miss)
                health -= 1;

            if (hitPrefab != null)
            {
                Text txt = Instantiate(hitPrefab);
                txt.text = BPerM.ReturnBeat.ToString();
            }
        }
    }
}
