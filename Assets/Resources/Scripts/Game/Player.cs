﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, ControllerInterface
{

    [Header("Acceleration Values")]
    [SerializeField] float torque = 1;
    [SerializeField] float maxTorque = 3f;
    [SerializeField] float minTorque = 1f;
    [SerializeField] float zeroPointTorque = 0;
    [SerializeField] float slowingTorque = 1;

    [Header("Side Movement Values")]
    [SerializeField] float sideSpeed = 1;
    [SerializeField] float maxSideSpeed = 3f;
    [SerializeField] float slowFactor = 1f;
    [SerializeField] [Range(0, 90)] float tiltAmount;
    [SerializeField] float attackDuration;

    Rigidbody rb;

    [Header("Attack and Body Components")]
    [SerializeField] Transform tiltObject;
    [SerializeField] GameObject leftAttack;
    [SerializeField] GameObject rightAttack;

    float currentTilt = 0;
    float tiltangle;

    bool sideSlow = false;
    bool forwardSlow = false;

    float attackTime;

    bool grounded = false;

    float accel;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        leftAttack.SetActive(false);
        rightAttack.SetActive(false);
    }

    void Update()
    {
        if (!sideSlow)
        {
            if (Mathf.Abs(rb.velocity.x) > maxSideSpeed)
            {
                float vert = rb.velocity.y;
                float hor = rb.velocity.x;
                float forward = rb.velocity.z;

                rb.velocity = Vector3.right * (hor / Mathf.Abs(hor) * maxSideSpeed) + Vector3.up * vert + Vector3.forward * forward;
            }
        }
        else
        {
            float hor = rb.velocity.x;
            rb.velocity += -Vector3.right * hor * Time.deltaTime * slowFactor;
        }


        if (attackTime >= 0)
        {
            attackTime -= Time.deltaTime;
        }
        else
        {
            leftAttack.SetActive(false);
            rightAttack.SetActive(false);
        }

        tiltangle = currentTilt + rb.velocity.x / maxSideSpeed * -tiltAmount;
        tiltObject.rotation = Quaternion.Euler(0.0f, 0.0f, tiltangle);

        Vector3 vel = rb.velocity;
        vel.z = zeroPointTorque * Time.deltaTime;

        rb.velocity = vel;

        sideSlow = true;
        forwardSlow = true;
    }

    public void Accelerate()
    {
    }

    public void Brake()
    {
    }

    public void Left()
    {
        rb.AddForce(-Vector3.right * sideSpeed, ForceMode.Impulse);
        sideSlow = false;
    }

    public void LeftAttack()
    {
        if (attackTime <= 0)
        {
            leftAttack.SetActive(true);
            attackTime = attackDuration;
        }
    }

    public void Right()
    {
        rb.AddForce(Vector3.right * sideSpeed, ForceMode.Impulse);
        sideSlow = false;
    }

    public void RightAttack()
    {
        if (attackTime <= 0)
        {
            rightAttack.SetActive(true);
            attackTime = attackDuration;
        }
    }

    void Grounded()
    {
        if (Physics.Raycast(transform.position, -Vector3.up, 0.5f * Mathf.Abs(Mathf.Sin(tiltangle))))
            grounded = true;
        else
            grounded = false;
    }
}