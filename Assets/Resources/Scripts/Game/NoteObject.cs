﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteObject : MonoBehaviour
{
    public float expireTime = 2f;

    bool canBePressed;

    bool delete;
    float time;

    // Start is called before the first frame update
    void Start()
    {
        delete = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (delete)
        {
            time += Time.deltaTime;

            if (time >= expireTime)
                Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Activator")
        {
            canBePressed = true;
            Debug.Log("Ayy");
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Activator")
        {
            canBePressed = false;
            delete = true;
        }
    }
}
