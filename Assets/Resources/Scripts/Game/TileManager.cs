﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour
{
    public GameObject[] tilePrefabs;

    Transform playerTransform;
    float spawnZ = 0.0f;
    float tileLength = 10;
    [SerializeField] int maxAmountTiles = 7;

    List<GameObject> tiles;

    // Start is called before the first frame update
    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        tiles = new List<GameObject>();

        for (int i = 0; i < maxAmountTiles; i++)
        {
            SpawnTile();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (playerTransform.position.z > (spawnZ - maxAmountTiles * tileLength))
        {
            SpawnTile();
        }

        if (tiles.Count > maxAmountTiles + 1)
        {
            Destroy(tiles[0]);
            tiles.RemoveAt(0);
        }
    }

    void SpawnTile(int index = -1)
    {
        GameObject temp;
        temp = Instantiate(tilePrefabs[0]);
        temp.transform.SetParent(transform);
        temp.transform.position = Vector3.forward * spawnZ;
        spawnZ += tileLength;

        tiles.Add(temp);
    }
}
