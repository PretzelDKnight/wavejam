﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] GameObject enemyPrefab;
    [SerializeField] float startTime;
    [SerializeField] float maxNumEnemies = 5;
    [SerializeField] float enemySpawnInterval = 1.5f;

    List<GameObject> enemies;

    float time;
    float startDelay;

    Transform player;

    // Start is called before the first frame update
    void Start()
    {
        enemies = new List<GameObject>();
        startDelay = 0;
        time = 0;

        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (startDelay >= startTime)
        {
            RefreshList();

            time += Time.deltaTime;

            if (enemies.Count < maxNumEnemies && time >= enemySpawnInterval)
            {
                enemies.Add(Instantiate(enemyPrefab, player.transform.position + new Vector3(Random.Range(-4, 4), 1, -8f), Quaternion.identity));
                time = 0;
            }
        }
        else
            startDelay += Time.deltaTime;
    }

    void RefreshList()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i] == null)
            {
                enemies.RemoveAt(i);
                i--;
            }
        }
    }
}
