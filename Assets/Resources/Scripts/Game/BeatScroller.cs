﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatScroller : MonoBehaviour
{
    public float bpm;

    public bool hasStarted;

    [SerializeField] Transform target;

    [SerializeField] GameObject notePrefab;
    [SerializeField] int maxNoteAmount = 7;
    [SerializeField] int noteLength = 80;

    List<GameObject> notes;

    float spawnX;


    // Start is called before the first frame update
    void Start()
    {
        notes = new List<GameObject>();
        spawnX = bpm;

        for (int i = 0; i < maxNoteAmount; i++)
        {
            SpawnNote();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasStarted)
        {
            if (Input.anyKeyDown)
                hasStarted = true;
        }
        else
        {
            transform.position -= new Vector3(noteLength * Time.deltaTime, 0f , 0f);
        }

        if (target.position.x > (notes[notes.Count - 1].transform.position.x - maxNoteAmount * noteLength))
        {
            SpawnNote();
        }
    }

    void SpawnNote()
    {
        GameObject temp;
        temp = Instantiate(notePrefab);
        temp.transform.SetParent(transform);
        temp.transform.localPosition = Vector3.right * (bpm + spawnX);
        spawnX += bpm;

        notes.Add(temp);
    }
}
