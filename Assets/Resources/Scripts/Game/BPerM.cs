﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Beat
{
    Miss,Hit
}

public class BPerM : MonoBehaviour
{
    static public BPerM bPerM = null;

    static Beat beat;

    [SerializeField] float bpm;

    float beatInterval;
    float beatIntervalD8;
    float beatTimer;
    float beatTimerD8;

    static bool beatFull;
    static bool beatD8;
    static int beatCountFull;
    static int beatCountD8;

    // Start is called before the first frame update
    void Start()
    {
        if (bPerM != null)
            Destroy(gameObject);
        else
            bPerM = this;
    }

    // Update is called once per frame
    void Update()
    {
        BeatDetection();
    }

    static public Beat ReturnBeat
    {
        get { return beat; }
    }

    void BeatDetection()
    {
        // Full beat
        beatFull = false;
        beatInterval = 60 / bpm;
        beatTimer += Time.deltaTime;

        if (beatTimer >= beatInterval)
        {
            beatTimer -= beatInterval;
            beatFull = true;
            beatCountFull++;
        }

        // Beat divided by 8
        beatD8 = false;
        beatIntervalD8 = beatInterval / 8;
        beatTimerD8 += Time.deltaTime;

        if (beatTimerD8 >= beatIntervalD8)
        {
            beatTimerD8 -= beatIntervalD8;
            beatD8 = true;
            beatCountD8++;
        }
    }
}
